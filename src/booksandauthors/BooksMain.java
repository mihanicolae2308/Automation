/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package booksandauthors;

/**
 *
 * @author mnicolae
 */
public class BooksMain {
    public static void main(String[] args) {
        Author bookAuthor = new Author("Mihai Nicolae", "mihai@office.com");
        Book newBook = new Book("Adventure Book", 2017, bookAuthor, 204.5);
        
        System.out.println("Book '" + newBook.getName() + "' , price " + newBook.getPrice() + "$, by " + newBook.getAuthor().getName() + ", published in year " + 
                newBook.getYear() + ".");
    }
    
}
