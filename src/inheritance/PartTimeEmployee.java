/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inheritance;

/**
 *
 * @author mnicolae
 */
public class PartTimeEmployee extends Employee {
    
    private double salary = super.getSalary() * 75/100;
    
    public PartTimeEmployee(String firstName, String lastName, String address, String city, String country, String birthDate) {
        super(firstName, lastName, address, city, country, birthDate);
    }

    @Override
    public double getSalary() {
        return salary;
    }
    
    @Override
    public void print() {
        System.out.println("This is the part time employee named " + this.getFirstName() + " " + this.getLastName() + " born on " + this.getBirthDate()
        + " with a salary of " + this.getSalary());
    }
}
