/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inheritance;

/**
 *
 * @author mnicolae
 */
public class Persons {

    public static void main(String[] args) {
        Person person = new Person("Vasile", "Ionescu", "Str Unirii, nr 23", "Slobozia", "Romania", "12-apr-1987");
        Employee employee = new Employee("George", "Vasilescu", "Str Eminescu, nr 12", "Ploiesti", "Romania", "21-aug-1980");
        PartTimeEmployee ptEmployee = new PartTimeEmployee("Mihai", "Popa", "Bd Primaverii, nr 2", "Bucuresti", "Romania", "01-mar-1990");
        
        person.print();
        employee.print();
        ptEmployee.print();
    }
    
}
