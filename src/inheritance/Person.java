/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inheritance;

/**
 *
 * @author mnicolae
 */

public class Person {
    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String country;
    private String birthDate;

    public Person(String firstName, String lastName, String address, String city, String country, String birthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.country = country;
        this.birthDate = birthDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }
    
    public void print() {
        System.out.println("This is the person named " + this.getFirstName() + " " + this.getLastName() + " born on " + this.getBirthDate());
    }
}
