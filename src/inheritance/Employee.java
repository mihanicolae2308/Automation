/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inheritance;

/**
 *
 * @author mnicolae
 */
public class Employee extends Person {
    private double salary = 5000;

    public Employee(String firstName, String lastName, String address, String city, String country, String birthDate) {
        super(firstName, lastName, address, city, country, birthDate);
    }

    public double getSalary() {
        return salary;
    }
    
    @Override
    public void print() {
        System.out.println("This is the employee named " + this.getFirstName() + " " + this.getLastName() + " born on " + this.getBirthDate()
        + " with a salary of " + this.getSalary());
    }
    
}
