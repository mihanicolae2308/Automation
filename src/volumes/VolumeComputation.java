package volumes;

/**
 * Created by Mihai on 06-Aug-17.
 */
public class VolumeComputation {

    public static final double PI = 3.14;

    public static double volume(double  radius, double piConstant) {
        return (4 * piConstant * Math.pow(radius, 3))/3;
    }

    public static double volume(double length) {
        return Math.pow(length, 3);
    }

    public static int volume(int length, int width, int height) {
        return length * width * height / 3;
    }

    public static double volume(double  radius, double piConstant, double height) {
        return piConstant * Math.pow(radius, 2) * height;
    }

    public static float volume(int  radius, int height) {
        return (float) (PI * radius * radius * height / 3);
    }

    public static void main(String[] args) {
        double sphereRadius = 4;
        System.out.println("The volume of the sphere with a radius of " + sphereRadius + " is " + volume(sphereRadius, PI));

        double cubeEdge = 5;
        System.out.println("The volume of the cube with an edge of " + cubeEdge + " is " + volume(cubeEdge));

        int pyramidLength = 10;
        int pyramidWidth = 7;
        int pyramidHeight = 6;

        System.out.println("The volume of the pyramid with a length of " + pyramidLength +
                " a width of " + pyramidWidth + " and a height of " + pyramidHeight + " is " + volume(pyramidLength, pyramidWidth, pyramidHeight));

        double cylinderRadius = 6;
        double cylinderHeight = 10;

        System.out.println("The volume of the cylinder with a radius of " + cylinderRadius + " and a height of " + cylinderHeight + " is "
                + volume(cylinderRadius, PI, cylinderHeight));

        int coneRadius = 8;
        int coneHeight = 6;

        System.out.println("The volume of the cone with a radius of " + coneRadius + " and a height of " + coneHeight + " is "
                + volume(coneRadius, coneHeight));
    }

}
