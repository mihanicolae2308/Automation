package com.company;

public class Calculator {

    public static void calculateSum(int a) {
        int sum = 0;
        for (int i=1; i<=a; i++) {
            sum += i;
        }
        System.out.println("The sum of the first " + a + " numbers is " + sum);
    }

    public static void calculateSumFormula(int a) {
        System.out.println("The sum of the first " + a + " numbers calculated with a formula is " + (a * (a+1))/2);
    }

    public static void checkPalindrome(String a) {
        boolean isPalindrome = true;
        for (int i = 0; i < a.length(); i++) {
            if (a.charAt(i) != a.charAt(a.length() -1 - i)) {
                isPalindrome = false;
            }
        }
        if (isPalindrome) {
            System.out.println(a + " is palindrome.");
        } else {
                System.out.println(a + " is not palindrome.");
            }
    }

    public static boolean isPrime (int a) {
        if (a == 2) return true;

        for (int i = 2; i<= a/2; i++) {
            if (a % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void primeNumbers (int a) {
        System.out.print("The list of prime numbers between 0 and " + a + " is: ");
        for (int i = 1; i <= a; i++) {
            if (isPrime(i)) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }



    public static void main(String[] args) {
        calculateSum(100);
        calculateSumFormula(100);

        if (args.length == 3) {
            checkPalindrome(args[0]);
            checkPalindrome(args[2]);

            int x = Integer.parseInt(args[0]);
            int y = Integer.parseInt(args[2]);

            primeNumbers(x);
            primeNumbers(y);

            float a = Float.parseFloat(args[0]);
            float b = Float.parseFloat(args[2]);
            float result = 0;


            String operator = args[1];

            switch(operator) {
                case "+":
                    result = a + b;
                    break;
                case "-":
                    result = a - b;
                    break;
                case "*":
                    result = a * b;
                    break;
                case "/":
                    result = a / b;
                    break;
                default:
                    System.err.println("Invalid operator!");
                    System.exit(1);
            }

            System.out.println("Result of " + a + " " + operator + " " + b + " is: " + result);

        } else {

            System.err.println("3 args are needed!");

        }
    }
}
